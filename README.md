# JWST-pan

Pan / Zoom / Rotate between multiple images (e.g. into the Cosmic Cliffs of the Carina Nebula).

This is a record of my approach for transitioning between images to show zoom context for the Cosmic Cliffs image from JWST (2022-031); I was inspired to attempt this by [this Reddit post](https://www.reddit.com/r/space/comments/vy8u7x/a_progress_of_images_taking_us_from_an_ground/). There are 184 frames in the final movie, including four static 5-frame transitions from one source image to another image, making approximately 164 frames of zoom, each zooming approximately 1/95% each time (with some slight adjustments to make the transitions fit properly). This means that the final image is approximately 4500X zoom.

Getting the transitions looking reasonable involved a lot of trial and error. It begins with working out the approximate start and end centre points, approximating the angle transition, then guessing at the number of frames required to get the starting image zoomed in to the required size. After I get an approximate result, I copy the final transformation command into the console, and tweak it many times while checking the next starting image, until it looks right. Then I regenerate all the transformation images.

This repository contains the R code for generating one-liner ImageMagick commands for the image frames, and transition frames used for switching from one image to the next:

# Image Sources

## ESO / [Babak A. Tafreshi](https://www.facebook.com/Babak.A.Tafreshi) (potw1548a); CC-BY-SA-4.0
 - https://www.eso.org/public/images/potw1548a/
 - https://commons.wikimedia.org/wiki/File:Starbirth_over_ALMA.jpg
## Harel Boren [Carina Nebula]; CC-BY-SA-4.0
 - https://www.pbase.com/boren/image/151851961/original
## MPG/ESO (eso1207a); CC-BY-SA-4.0
 - https://www.eso.org/public/images/eso1207a/
## JWST (2022-031); PD-NASA
 - https://webbtelescope.org/contents/media/images/2022/031/01G77PKB8NKR7S8Z6HBXMYATGJ
 - https://stsci-opo.org/STScI-01G7WCHVJ1VXPW5CX5DSVE0W1F.png

# Generating The Final Movie

## for ffmpeg help, see here:
https://gist.github.com/katattakd/19363e3ef458ea206aec141ad9d8b382

## The magic command for Twitter:
ffmpeg -framerate 10 -i frame%03d.png -an -max_muxing_queue_size 1024 -t 2:19 -map_metadata -1 -vcodec libx264 -g 180 -keyint_min 180 -pix_fmt yuv420p -acodec n>

## This seems to work as well (eventually, once the Twitter cache sorts itself out):
ffmpeg -framerate 10 -i frame%03d.png -vcodec libx264 -pix_fmt yuv420p -acodec none animated.mp4

# Frame Scripts

## frame 0 (left of first image)
    convert ../Starbirth_over_ALMA.jpg -crop 4096x4096+1024+0 -resize 750x750 frame000.png

## frames 1-38 (zoom and rotate to match Boren image)
```
fm <- 39;
fs <- 0:fm;
w <- 4096 * (0.9501^fs);
scale <- 750 / w;
csf <- rev(1 - ((scale - min(scale)) / (max(scale)-min(scale))));
xofs <- (3072) * (1 - csf) + (1845) * (csf);
yofs <- (2048) * (1 - csf) + (1531) * (csf);
rtang <- (fs)/fm * -24.5;
cat(sprintf(paste("convert ../Starbirth_over_ALMA.jpg",
                   "-define distort:viewport=750x750+0+0",
                   "-distort SRT %0.1f,%0.1f,%0.3f,%0.3f,375,375 frame%03d.png"),
            xofs, yofs, scale, rtang, 0 + fs), sep="\n");
```

## frames 39-43 (cross fade into Harel Boren / 151851961)
*[frame 39 from the previous phase was renamed to "phase1_end.png"]*

```
pct <- (1 - ((1:5) / 5)) * 100;
cat(sprintf("composite -dissolve %0.1f phase1_end.png phase3_start.png frame%03d.png",
            pct, 39:43), sep="\n");
```

## [notes: frame numbers from 52 onwards preserved for legacy reasons]

## frames 44-75 (pan & zoom to match ESO image)
```
fm <- 32;
fs <- 0:fm;
w <- 2566 * (0.951^(fs));
scale <- 750 / w;
csf <- rev(1 - ((scale - min(scale)) / (max(scale)-min(scale))));
fs <- tail(fs, -1);
scale <- tail(scale, -1);
csf <- tail(csf, -1);
xofs <- (1283) * (1 - csf) + (2208) * (csf);
yofs <- (1283) * (1 - csf) + (2175) * (csf);
rtang <- (fs)/fm * -2;
cat(sprintf(paste("convert ../Boren_Carina_Nebula_2566x2566.jpg",
                   "-define distort:viewport=750x750+0+0",
                   "-distort SRT %0.1f,%0.1f,%0.3f,%0.3f,375,375 frame%03d.png"),
            xofs, yofs, scale, rtang, 43 + fs), sep="\n");
```

## frames 76-80
*[frame 83 from the previous phase was renamed to "phase3_end.png"]*

```
pct <- (1 - ((1:5) / 5)) * 100;
cat(sprintf("composite -dissolve %0.1f phase3_end.png phase4_start.png frame%03d.png",
            pct, 76:80), sep="\n");
```

## frames 81-116
```
fm <- 36;
fs <- 0:fm;
w <- 6663 * (0.9501^(fs));
scale <- 750 / w;
csf <- rev(1 - ((scale - min(scale)) / (max(scale)-min(scale))));
fs <- tail(fs, -1);
scale <- tail(scale, -1);
csf <- tail(csf, -1);
xofs <- (3331.5) * (1 - csf) + (5603) * (csf);
yofs <- (3331.5) * (1 - csf) + (3048) * (csf);
rtang <- (fs)/fm * 13 + 90;
cat(sprintf(paste("convert ../eso1207a.tif",
                   "+profile '*'",
                   "-define distort:viewport=750x750+0+0",
                   "-distort SRT %0.1f,%0.1f,%0.3f,%0.3f,375,375 frame%03d.png"),
            xofs, yofs, scale, rtang, 80 + fs), sep="\n");
```

## frames 117-121 (cross fade into JWST 2022-031
*[frame 123 from the previous phase was renamed to "phase4_end.png"]*

```
pct <- (1 - ((1:5) / 5)) * 100;
cat(sprintf("composite -dissolve %0.1f phase4_end.png phase5_start.png frame%03d.png",
            pct, 117:121), sep="\n");
```

## frames 122-184 (zoom into sub-pixel resolution for JWST image)
```
fm <- 63;
fs <- 0:fm;
w <- 8441 * (0.95^(fs));
scale <- 750 / w;
csf <- rev(1 - ((scale - min(scale)) / (max(scale)-min(scale))));
fs <- tail(fs, -1);
scale <- tail(scale, -1);
csf <- tail(csf, -1);
xofs <- (4220.5) * (1 - csf) + (2843) * (csf);
yofs <- (4220.5) * (1 - csf) + (3197) * (csf);
rtang <- (1:fm)/fm * 0;
cat(sprintf(paste("convert ../JWST_Cosmic_Cliffs.png",
                   "+profile '*'",
                   "-define distort:viewport=750x750+0+0",
                   "-distort SRT %0.1f,%0.1f,%0.3f,%0.3f,375,375 frame%03d.png"),
            xofs, yofs, scale, rtang, 121 + 1:fm), sep="\n");
```

## License

Please see the source images for creative licenses. Feel free to use this code for any purpose; this record is here only so that I can more easily adapt it in the future for other purposes where desired.

 - David Eccles (gringer), 2022
